﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityUtil.ModularInputSystem.InputManager_daemon3000;

namespace UnityUtil.ModularInputSystem
{
    //
    // UC = Unity Classic
    //
    public class UCInputManager : _InputManager
    {
        protected void Awake()
        {
            AddInputSetting("TEST");
        }

        public override bool GetButton(UniAction action)
        {
            if (!Input.GetButton(action.ToString()))
            {
                AddInputSetting(action.ToString());
                return false;
            }

            return true;
        }

        public override bool GetButtonDown(UniAction action)
        {
            if (!Input.GetButtonDown(action.ToString()))
            {
                AddInputSetting(action.ToString());
                return false;
            }

            return true;
        }

        public override bool GetButtonUp(UniAction action)
        {
            if (!Input.GetButtonUp(action.ToString()))
            {
                AddInputSetting(action.ToString());
                return false;
            }

            return true;
        }

        public override float GetAxis(UniAction action)
        {
            var axisValue =  .0f;

            try
            {
                axisValue = Input.GetAxis(UniAction.Jump.ToString());
                Debug.Log(axisValue);
            }
            catch (ArgumentException)
            {
                Debug.Log("Axis does not exist!");
                AddInputSetting(action.ToString());
            }

            return axisValue;
        }

        public override float GetAxisRaw(UniAction action)
        {
            var axisValue = .0f;

            try
            {
                axisValue = Input.GetAxisRaw(UniAction.Jump.ToString());
                Debug.Log(axisValue);
            }
            catch (ArgumentException)
            {
                Debug.Log("Axis does not exist!");
                AddInputSetting(action.ToString());
            }

            return axisValue;
        }

#if UNITY_EDITOR
        private void AddInputSetting(string actionName) => _InputSerializer.SerializeNewAction(actionName);
#endif
    }
}
