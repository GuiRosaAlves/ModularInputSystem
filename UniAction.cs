﻿namespace UnityUtil.ModularInputSystem
{
    public enum UniAction //Universal Actions
    {
        None,
        MoveHorizontal,
        MoveVertical,
        Jump,
        Attack,
        SubAttack,
        Dash,
        Run,
        Pause,
        Count
    }
}