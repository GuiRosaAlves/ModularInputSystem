﻿using System.IO;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityUtil.EditorScript;

namespace UnityUtil.ModularInputSystem
{
    public class InputActionToJson : MonoBehaviour
    {
        [SerializeField] private InputActionAsset _inputActionAsset;
        [SerializeField] private bool usePersistentDataPath = true;
        [SerializeField] private string _filePath = "";
        [SerializeField] private string _fileName = "InputActionMap";
        [TextArea][SerializeField] private string _jsonMap;

        [Button("SaveToJSON", "Create JSON", false)] [SerializeField] private string _createJSON;

        public void SaveToJSON()
        {
            InputActionAsset actionAssetToUse;
            if (_inputActionAsset == null)
            {
                actionAssetToUse = _inputActionAsset;
            }
            else
            {
                // Create an Action Asset.
                InputActionAsset _newActionAsset = ScriptableObject.CreateInstance<InputActionAsset>();
                var gameplayMap = new InputActionMap("gameplay");
                _newActionAsset.AddActionMap(gameplayMap);
                var lookAction = gameplayMap.AddAction("look", InputActionType.Button);

                actionAssetToUse = _newActionAsset;
            }

            var actionMaps = InputActionMap.ToJson(actionAssetToUse.actionMaps);
            if (usePersistentDataPath || string.IsNullOrEmpty(_filePath))
            {
                System.IO.File.WriteAllText(Application.persistentDataPath + "/" + _fileName +".json", actionMaps);
            }
            else if(Directory.Exists(_filePath))
            {
                System.IO.File.WriteAllText(_filePath + "/" + _fileName+".json", actionMaps);
            }
            else
            {
                Debug.Log("Invalid File path: "+_filePath);
            }
        }

        public void LoadJson()
        {
            _jsonMap = System.IO.File.ReadAllText(Application.persistentDataPath + "/InputActionMap.json");
            Debug.Log(_jsonMap);
        }

        // public void Update()
        // {
        //     Debug.Log(KeyCode.S);
        // }
    }
}
