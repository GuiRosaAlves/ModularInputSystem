﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace UnityUtil.ModularInputSystem
{
    //
    // UN = Unity New
    //
    public class UNInputManager : _InputManager
    {
        private static int keyboardKeys = 256;
        private static int mouseKeys = 5;

        public InputActionAsset _ipt;

        private void OnEnable()
        {
            _ipt.Enable();
        }
        private void OnDisable()
        {
            _ipt.Disable();
        }

        public override bool GetButton(UniAction action)
        {
            InputAction inputAction = _ipt.FindAction(action.ToString());
            return (inputAction == null) ? false : inputAction.ReadValue<bool>();
        }


        public override bool GetButtonDown(UniAction action)
        {
            InputAction inputAction = _ipt.FindAction(action.ToString());
            return (inputAction == null) ? false : inputAction.ReadValue<bool>();
        }

        public override bool GetButtonUp(UniAction action)
        {
            InputAction inputAction = _ipt.FindAction(action.ToString());
            return (inputAction == null) ? false : inputAction.ReadValue<bool>();
        }

        public override float GetAxis(UniAction action)
        {
            InputAction inputAction = _ipt.FindAction(action.ToString());
            return (inputAction == null) ? 0f : inputAction.ReadValue<float>();
        }

        public override float GetAxisRaw(UniAction action)
        {
            Debug.Log("Method is not compatible with new Input System, you can achieve the same result by adding properties in the Input Actions Editor.");
            return GetAxis(action);
        }
    }
}
