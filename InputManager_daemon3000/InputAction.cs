﻿// Decompiled with JetBrains decompiler
// Type: UnityInputConverter.InputAction
// Assembly: UnityInputConverter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C78EFC52-C5FE-4647-973C-232B9F6B0029
// Assembly location: C:\Users\guilh\Desktop\InputManager-master\Assets\InputManager\Source\Editor\Library\UnityInputConverter.dll

using System.Collections.Generic;

namespace UnityUtil.ModularInputSystem.InputManager_daemon3000
{
  [System.Serializable]
  public class InputAction
  {
    public string Name;
    public string Description;
    public List<InputBinding> Bindings;

    public InputAction()
    {
      this.Name = "New Action";
      this.Description = "";
      this.Bindings = new List<InputBinding>();
    }

    public InputBinding CreateNewBinding()
    {
      InputBinding inputBinding = new InputBinding();
      this.Bindings.Add(inputBinding);
      return inputBinding;
    }

    public InputBinding CreateNewBinding(InputBinding source)
    {
      InputBinding inputBinding = InputBinding.Duplicate(source);
      this.Bindings.Add(inputBinding);
      return inputBinding;
    }
  }
}
