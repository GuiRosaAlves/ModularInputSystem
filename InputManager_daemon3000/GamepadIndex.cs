﻿
using System;

namespace UnityUtil.ModularInputSystem.InputManager_daemon3000
{
	[Serializable]
	public enum GamepadIndex
	{
		GamepadOne = 0, GamepadTwo, GamepadThree, GamepadFour
	}
}
