﻿
using System;

namespace UnityUtil.ModularInputSystem.InputManager_daemon3000
{
	[Serializable]
	public enum GamepadButton
	{
		LeftStick,
		RightStick,
		LeftBumper,
		RightBumper,
		DPadUp,
		DPadDown,
		DPadLeft,
		DPadRight,
		Back,
		Start,
		ActionBottom,
		ActionRight,
		ActionLeft,
		ActionTop,
	}
}
