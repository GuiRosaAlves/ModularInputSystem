﻿// Decompiled with JetBrains decompiler
// Type: UnityInputConverter.InputSaverXML
// Assembly: UnityInputConverter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C78EFC52-C5FE-4647-973C-232B9F6B0029
// Assembly location: C:\Users\guilh\Desktop\InputManager-master\Assets\InputManager\Source\Editor\Library\UnityInputConverter.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace UnityUtil.ModularInputSystem.InputManager_daemon3000
{
  internal class InputSaverXML
  {
    private const int VERSION = 2;
    private string m_filename;
    private Stream m_outputStream;
    private StringBuilder m_output;

    public InputSaverXML(string filename)
    {
      this.m_filename = filename ?? throw new ArgumentNullException(nameof (filename));
      this.m_outputStream = (Stream) null;
      this.m_output = (StringBuilder) null;
    }

    public InputSaverXML(Stream stream)
    {
      this.m_outputStream = stream ?? throw new ArgumentNullException(nameof (stream));
      this.m_filename = (string) null;
      this.m_output = (StringBuilder) null;
    }

    public InputSaverXML(StringBuilder output)
    {
      this.m_output = output ?? throw new ArgumentNullException(nameof (output));
      this.m_filename = (string) null;
      this.m_outputStream = (Stream) null;
    }

    private XmlWriter CreateXmlWriter(XmlWriterSettings settings)
    {
      if (this.m_filename != null)
        return XmlWriter.Create(this.m_filename, settings);
      if (this.m_outputStream != null)
        return XmlWriter.Create(this.m_outputStream, settings);
      return this.m_output != null ? XmlWriter.Create(this.m_output, settings) : (XmlWriter) null;
    }

    public void Save(List<ControlScheme> controlSchemes)
    {
      using (XmlWriter xmlWriter = this.CreateXmlWriter(new XmlWriterSettings()
      {
        Encoding = Encoding.UTF8,
        Indent = true
      }))
      {
        xmlWriter.WriteStartDocument(true);
        xmlWriter.WriteStartElement("Input");
        xmlWriter.WriteAttributeString("version", 2.ToString());
        xmlWriter.WriteElementString("PlayerOneScheme", controlSchemes.Count > 0 ? controlSchemes[0].UniqueID : "");
        xmlWriter.WriteElementString("PlayerTwoScheme", "");
        xmlWriter.WriteElementString("PlayerThreeScheme", "");
        xmlWriter.WriteElementString("PlayerFourScheme", "");
        foreach (ControlScheme controlScheme in controlSchemes)
          this.WriteControlScheme(controlScheme, xmlWriter);
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndDocument();
      }
    }

    private void WriteControlScheme(ControlScheme scheme, XmlWriter writer)
    {
      writer.WriteStartElement("ControlScheme");
      writer.WriteAttributeString("name", scheme.Name);
      writer.WriteAttributeString("id", scheme.UniqueID);
      foreach (InputAction action in scheme.Actions)
        this.WriteInputAction(action, writer);
      writer.WriteEndElement();
    }

    private void WriteInputAction(InputAction action, XmlWriter writer)
    {
      writer.WriteStartElement("Action");
      writer.WriteAttributeString("name", action.Name);
      writer.WriteElementString("Description", action.Description);
      foreach (InputBinding binding in action.Bindings)
        this.WriteInputBinding(binding, writer);
      writer.WriteEndElement();
    }

    private void WriteInputBinding(InputBinding binding, XmlWriter writer)
    {
      writer.WriteStartElement("Binding");
      writer.WriteElementString("Positive", binding.Positive.ToString());
      writer.WriteElementString("Negative", binding.Negative.ToString());
      writer.WriteElementString("DeadZone", binding.DeadZone.ToString());
      writer.WriteElementString("Gravity", binding.Gravity.ToString());
      writer.WriteElementString("Sensitivity", binding.Sensitivity.ToString());
      writer.WriteElementString("Snap", binding.Snap.ToString().ToLower());
      writer.WriteElementString("Invert", binding.Invert.ToString().ToLower());
      writer.WriteElementString("Type", binding.Type.ToString());
      writer.WriteElementString("Axis", binding.Axis.ToString());
      writer.WriteElementString("Joystick", binding.Joystick.ToString());
      writer.WriteEndElement();
    }
  }
}
