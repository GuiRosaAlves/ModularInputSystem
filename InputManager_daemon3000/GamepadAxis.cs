﻿
using System;

namespace UnityUtil.ModularInputSystem.InputManager_daemon3000
{
	[Serializable]
	public enum GamepadAxis
	{
		LeftThumbstickX,
		LeftThumbstickY,
		RightThumbstickX,
		RightThumbstickY,
		DPadX,
		DPadY,
		LeftTrigger,
		RightTrigger
	}
}
