﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using YamlDotNet.Serialization;

namespace UnityUtil.ModularInputSystem.InputManager_daemon3000
{
#if UNITY_EDITOR
	[InitializeOnLoad]
	public static class _InputSerializer
	{
		//TODO: Since unity doesn't provide access to the InputManager class in order to
		//Deserialize and Serialize the class we have to simulate it using an Object type Dictionary
		//Create a alternative for ControlScheme Class
		//Create Universal Actions Enum - Done
		//Make it so the actions are added to the Input Manager dinamically as the script requires the action

		public static readonly string InputSettingsPath;
		public static Dictionary<object, object> InputSettingsCache = null;

		static _InputSerializer()
		{
			string path = Application.dataPath;
			path = path.Substring(0, (path.Length - 7)) + InputSettingsPath;
			InputSettingsPath = path + "/ProjectSettings/InputManager.asset";
			InputSettingsCache = (Dictionary<object,object>)DeserializeUnityInputManager();
			EditorApplication.playModeStateChanged += OnPlayModeStop;
		}

		[MenuItem("HotKey/Start Input Record %&s")]
		static void StartInputRecord()
		{
			ClearLog();
			//SerializeNewAction("Testt");
			Debug.Log("Starting Input Record");
			SerializeInputManager(InputSettingsPath);
			Debug.Log("Replaced old input manager!! Does it freaking work?");
		}

		[MenuItem("HotKey/Log Input Settings Cache")]
		static void LogSettingsCache()
		{
			ClearLog();
			List<object> m_Axes = (List<object>)
				((IDictionary<object, object>)InputSettingsCache[(object)"InputManager"])[(object)"m_Axes"];

			for (int i = 0; i < m_Axes.Count; i++) //Debugging
			{
				IDictionary<object, object> axisData = (IDictionary<object, object>)m_Axes[i];
				Debug.Log($"Action {i} name: {axisData[(object)"m_Name"]}");
			}
		}

		[MenuItem("HotKey/Reload Input Settings Cache")]
		static void ReloadSettingsCache()
		{
			ClearLog();
			EditorSceneManager.SaveScene(SceneManager.GetActiveScene(), "", false);
			InputSettingsCache = (Dictionary<object, object>)DeserializeUnityInputManager();
			LogSettingsCache();
		}

		public static IDictionary<object, object> DeserializeUnityInputManager()
		{
			IDictionary<object, object> dictionary = (IDictionary<object, object>) null;
			ControlScheme controlScheme = new ControlScheme("Unity-Imported");
			
			using (StreamReader streamReader = File.OpenText(InputSettingsPath))
			{
				streamReader.ReadLine();
				streamReader.ReadLine();
				streamReader.ReadLine();
				dictionary = new Deserializer().Deserialize<IDictionary<object, object>>((TextReader) streamReader);
			}
			
			if (dictionary == null || dictionary.Count == 0)
				throw new FormatException();
			foreach (IDictionary<object, object> axisData in (IEnumerable<object>)
				((IDictionary<object, object>) dictionary[(object) "InputManager"])[(object) "m_Axes"])
			{
				controlScheme.Actions.Add(ConvertUnityInputAxis(axisData));
			}

			return dictionary;
		}

		public static void SerializeInputManager(string destinationFile)
		{
			//Generate Default Unity Input Manager method from InputConverter Class
			Dictionary<string, object> dictionary1 = new Dictionary<string, object>();
			Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
			List<Dictionary<string, object>> dictionaryList = new List<Dictionary<string, object>>();
			dictionary1.Add("InputManager", (object) dictionary2);
			dictionary2.Add("m_ObjectHideFlags", (object) 0);
			dictionary2.Add("m_Axes", (object) dictionaryList);

			using (StreamWriter text = File.CreateText(destinationFile))
			{
				Serializer serializer = new Serializer();
				StringWriter stringWriter1 = new StringWriter();
				StringWriter stringWriter2 = stringWriter1;

				serializer.Serialize((TextWriter)stringWriter2, (object)InputSettingsCache);
				text.Write("%YAML 1.1\r\n%TAG !u! tag:unity3d.com,2011:\r\n--- !u!13 &1\r\n{0}",
					(object)stringWriter1.ToString());

				Debug.Log("Modified Input Settings");
			}
		}

		private static InputAction ConvertUnityInputAxis(IDictionary<object, object> axisData)
		{
			int type = ParseInt(axisData[(object) "type"].ToString());
			int num1 = ParseInt(axisData[(object) "axis"].ToString());
			int num2 = ParseInt(axisData[(object) "joyNum"].ToString(), 1) - 1;
			InputAction inputAction = new InputAction()
			{
				Name = axisData[(object) "m_Name"].ToString()
			};
			InputBinding newBinding1 = inputAction.CreateNewBinding();
			newBinding1.Gravity = ParseFloat(axisData[(object) "gravity"].ToString());
			newBinding1.DeadZone = ParseFloat(axisData[(object) "dead"].ToString());
			newBinding1.Sensitivity = ParseFloat(axisData[(object) "sensitivity"].ToString());
			newBinding1.Snap = (uint) ParseInt(axisData[(object) "snap"].ToString()) > 0U;
			newBinding1.Invert = (uint) ParseInt(axisData[(object) "invert"].ToString()) > 0U;
			newBinding1.Positive = ConvertUnityKeyCode(axisData[(object) "positiveButton"]);
			newBinding1.Negative = ConvertUnityKeyCode(axisData[(object) "negativeButton"]);
			newBinding1.Type = ParseInputType(type);
			newBinding1.Axis = Clamp(num1, 0, 27);
			newBinding1.Joystick = Clamp(num2, 0, 11);
			KeyCode keyCode1 = ConvertUnityKeyCode(axisData[(object) "altPositiveButton"]);
			KeyCode keyCode2 = ConvertUnityKeyCode(axisData[(object) "altNegativeButton"]);
			if (newBinding1.Type == InputType.Button)
			{
				if (newBinding1.Positive != KeyCode.None && newBinding1.Negative != KeyCode.None)
					newBinding1.Type = InputType.DigitalAxis;
				if (keyCode1 != KeyCode.None && keyCode2 != KeyCode.None)
				{
					InputBinding newBinding2 = inputAction.CreateNewBinding(newBinding1);
					newBinding2.Positive = keyCode1;
					newBinding2.Negative = keyCode2;
					newBinding2.Type = InputType.DigitalAxis;
				}
				else if (keyCode1 != KeyCode.None)
				{
					InputBinding newBinding2 = inputAction.CreateNewBinding(newBinding1);
					newBinding2.Positive = keyCode1;
					newBinding2.Type = InputType.Button;
				}
			}
			return inputAction;
		}
	
		private static KeyCode ConvertUnityKeyCode(object value) => value != null ? KeyCodeConverter.StringToKey(value.ToString()) : KeyCode.None;
		
	
		private static int Clamp(int value, int min, int max)
		{
			if (value < min)
				return min;
			return value > max ? max : value;
		}
		private static float ParseFloat(string str, float defValue = 0.0f)
		{
			float result = defValue;
			return float.TryParse(str, NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out result) ? result : defValue;
		}

		private static int ParseInt(string str, int defValue = 0)
		{
			int result = defValue;
			return int.TryParse(str, out result) ? result : defValue;
		}

		private static InputType ParseInputType(int type, InputType defValue = InputType.Button)
		{
			switch (type)
			{
				case 0:
					return InputType.Button;
				case 1:
					return InputType.MouseAxis;
				case 2:
					return InputType.AnalogAxis;
				default:
					return defValue;
			}
		}

		public static void SerializeNewAction(string actionName)
		{
			LogSettingsCache();

			List<object> m_Axes = (List<object>)
				((IDictionary<object, object>)InputSettingsCache[(object)"InputManager"])[(object)"m_Axes"];

			if (IsActionInList(m_Axes, actionName))
				return;
			
			m_Axes.Add(GenerateAxis(actionName));
		}

		private static bool IsActionInList(IReadOnlyList<object> actionsList, string actionName)
		{
			for (int i = 0; i < actionsList.Count; i++)
			{
				var axisData = (IDictionary<object, object>) actionsList[i];
				
				if (axisData[(object)"m_Name"].Equals(actionName))
				{
					Debug.Log("Same Value Found");
					return true;
				}
			}
			return false;
		}
		
		private static void OnPlayModeStop(PlayModeStateChange state)
		{
			if (state == PlayModeStateChange.EnteredEditMode)
			{
				// EditorApplication.playModeStateChanged -= OnPlayModeStop;
				// AssetDatabase.SaveAssets();
			}
			if (state == PlayModeStateChange.ExitingEditMode)
			{
				// AssetDatabase.SaveAssets();
				//Debug.Log("Exiting Editor Mode");
				// EditorSceneManager.SaveScene(SceneManager.GetActiveScene(), "", false);
			}
		}

		private static void ClearLog()
		{
			var assembly = Assembly.GetAssembly(typeof(UnityEditor.Editor));
			var type = assembly.GetType("UnityEditor.LogEntries");
			var method = type.GetMethod("Clear");
			method.Invoke(new object(), null);
		}

		public static Dictionary<object, object> GenerateAxis(string name) => new Dictionary<object, object>()
		{
			{
				"serializedVersion",
				(object) 3
			},
			{
				"m_Name",
				(object) name
			},
			{
				"descriptiveName",
				(object) null
			},
			{
				"descriptiveNegativeName",
				(object) null
			},
			{
				"negativeButton",
				(object) null
			},
			{
				"positiveButton",
				(object) null
			},
			{
				"altNegativeButton",
				(object) null
			},
			{
				"altPositiveButton",
				(object) null
			},
			{
				"gravity",
				(object) 0
			},
			{
				"dead",
				(object) 0
			},
			{
				"sensitivity",
				(object) 1
			},
			{
				"snap",
				(object) 0
			},
			{
				"invert",
				(object) 0
			},
			{
				"type",
				(object) 0
			},
			{
				"axis",
				(object) 0
			},
			{
				"joyNum",
				(object) 0
			}
		};
	}
#endif
}
