﻿// Decompiled with JetBrains decompiler
// Type: UnityInputConverter.InputConverter
// Assembly: UnityInputConverter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C78EFC52-C5FE-4647-973C-232B9F6B0029
// Assembly location: C:\Users\guilh\Desktop\InputManager-master\Assets\InputManager\Source\Editor\Library\UnityInputConverter.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;
using YamlDotNet.Serialization;

namespace UnityUtil.ModularInputSystem.InputManager_daemon3000
{
  public class InputConverter
  {
    private const string INPUT_MANAGER_FILE_TEMPLATE = "%YAML 1.1\r\n%TAG !u! tag:unity3d.com,2011:\r\n--- !u!13 &1\r\n{0}";
    private const int SERIALIZED_VERSION = 3;
    private const int OBJECT_HIDE_FLAGS = 0;
    private const int BUTTON_TYPE = 0;
    private const int MOUSE_AXIS_TYPE = 1;
    private const int JOYSTICK_AXIS_TYPE = 2;

    public void ConvertUnityInputManager(string sourceFile, string destinationFile)
    {
      IDictionary<object, object> dictionary = (IDictionary<object, object>) null;
      ControlScheme controlScheme = new ControlScheme("Unity-Imported");
      using (StreamReader streamReader = File.OpenText(sourceFile))
      {
        streamReader.ReadLine();
        streamReader.ReadLine();
        streamReader.ReadLine();
        dictionary = new Deserializer().Deserialize<IDictionary<object, object>>((TextReader) streamReader);
      }
      if (dictionary == null || dictionary.Count == 0)
        throw new FormatException();
      foreach (IDictionary<object, object> axisData in (IEnumerable<object>) ((IDictionary<object, object>) dictionary[(object) "InputManager"])[(object) "m_Axes"])
        controlScheme.Actions.Add(this.ConvertUnityInputAxis(axisData));
      new InputSaverXML(destinationFile).Save(new List<ControlScheme>()
      {
        controlScheme
      });
    }

    private InputAction ConvertUnityInputAxis(IDictionary<object, object> axisData)
    {
      int type = this.ParseInt(axisData[(object) "type"].ToString());
      int num1 = this.ParseInt(axisData[(object) "axis"].ToString());
      int num2 = this.ParseInt(axisData[(object) "joyNum"].ToString(), 1) - 1;
      InputAction inputAction = new InputAction()
      {
        Name = axisData[(object) "m_Name"].ToString()
      };
      InputBinding newBinding1 = inputAction.CreateNewBinding();
      newBinding1.Gravity = this.ParseFloat(axisData[(object) "gravity"].ToString());
      newBinding1.DeadZone = this.ParseFloat(axisData[(object) "dead"].ToString());
      newBinding1.Sensitivity = this.ParseFloat(axisData[(object) "sensitivity"].ToString());
      newBinding1.Snap = (uint) this.ParseInt(axisData[(object) "snap"].ToString()) > 0U;
      newBinding1.Invert = (uint) this.ParseInt(axisData[(object) "invert"].ToString()) > 0U;
      newBinding1.Positive = this.ConvertUnityKeyCode(axisData[(object) "positiveButton"]);
      newBinding1.Negative = this.ConvertUnityKeyCode(axisData[(object) "negativeButton"]);
      newBinding1.Type = this.ParseInputType(type);
      newBinding1.Axis = this.Clamp(num1, 0, 27);
      newBinding1.Joystick = this.Clamp(num2, 0, 11);
      KeyCode keyCode1 = this.ConvertUnityKeyCode(axisData[(object) "altPositiveButton"]);
      KeyCode keyCode2 = this.ConvertUnityKeyCode(axisData[(object) "altNegativeButton"]);
      if (newBinding1.Type == InputType.Button)
      {
        if (newBinding1.Positive != KeyCode.None && newBinding1.Negative != KeyCode.None)
          newBinding1.Type = InputType.DigitalAxis;
        if (keyCode1 != KeyCode.None && keyCode2 != KeyCode.None)
        {
          InputBinding newBinding2 = inputAction.CreateNewBinding(newBinding1);
          newBinding2.Positive = keyCode1;
          newBinding2.Negative = keyCode2;
          newBinding2.Type = InputType.DigitalAxis;
        }
        else if (keyCode1 != KeyCode.None)
        {
          InputBinding newBinding2 = inputAction.CreateNewBinding(newBinding1);
          newBinding2.Positive = keyCode1;
          newBinding2.Type = InputType.Button;
        }
      }
      return inputAction;
    }

    private KeyCode ConvertUnityKeyCode(object value) => value != null ? KeyCodeConverter.StringToKey(value.ToString()) : KeyCode.None;

    public void GenerateDefaultUnityInputManager(string destinationFile)
    {
      Dictionary<string, object> dictionary1 = new Dictionary<string, object>();
      Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
      List<Dictionary<string, object>> dictionaryList = new List<Dictionary<string, object>>();
      dictionary1.Add("InputManager", (object) dictionary2);
      dictionary2.Add("m_ObjectHideFlags", (object) 0);
      dictionary2.Add("m_Axes", (object) dictionaryList);
      for (int axis = 0; axis < 3; ++axis)
        dictionaryList.Add(this.GenerateUnityMouseAxis(axis));
      for (int joystick = 1; joystick <= 11; ++joystick)
      {
        for (int axis = 0; axis < 28; ++axis)
          dictionaryList.Add(this.GenerateUnityJoystickAxis(joystick, axis));
      }
      using (StreamWriter text = File.CreateText(destinationFile))
      {
        Serializer serializer = new Serializer();
        StringWriter stringWriter1 = new StringWriter();
        StringWriter stringWriter2 = stringWriter1;
        Dictionary<string, object> dictionary3 = dictionary1;
        serializer.Serialize((TextWriter) stringWriter2, (object) dictionary3);
        text.Write("%YAML 1.1\r\n%TAG !u! tag:unity3d.com,2011:\r\n--- !u!13 &1\r\n{0}", (object) stringWriter1.ToString());
      }
    }

    public Dictionary<string, object> GenerateUnityMouseAxis(int axis) => new Dictionary<string, object>()
    {
      {
        "serializedVersion",
        (object) 3
      },
      {
        "m_Name",
        (object) string.Format("mouse_axis_{0}", (object) axis)
      },
      {
        "descriptiveName",
        (object) null
      },
      {
        "descriptiveNegativeName",
        (object) null
      },
      {
        "negativeButton",
        (object) null
      },
      {
        "positiveButton",
        (object) null
      },
      {
        "altNegativeButton",
        (object) null
      },
      {
        "altPositiveButton",
        (object) null
      },
      {
        "gravity",
        (object) 0
      },
      {
        "dead",
        (object) 0
      },
      {
        "sensitivity",
        (object) 1
      },
      {
        "snap",
        (object) 0
      },
      {
        "invert",
        (object) 0
      },
      {
        "type",
        (object) 1
      },
      {
        nameof (axis),
        (object) axis
      },
      {
        "joyNum",
        (object) 0
      }
    };

    public Dictionary<string, object> GenerateUnityJoystickAxis(int joystick, int axis) => new Dictionary<string, object>()
    {
      {
        "serializedVersion",
        (object) 3
      },
      {
        "m_Name",
        (object) string.Format("joy_{0}_axis_{1}", (object) (joystick - 1), (object) axis)
      },
      {
        "descriptiveName",
        (object) null
      },
      {
        "descriptiveNegativeName",
        (object) null
      },
      {
        "negativeButton",
        (object) null
      },
      {
        "positiveButton",
        (object) null
      },
      {
        "altNegativeButton",
        (object) null
      },
      {
        "altPositiveButton",
        (object) null
      },
      {
        "gravity",
        (object) 0
      },
      {
        "dead",
        (object) 0
      },
      {
        "sensitivity",
        (object) 1
      },
      {
        "snap",
        (object) 0
      },
      {
        "invert",
        (object) 0
      },
      {
        "type",
        (object) 2
      },
      {
        nameof (axis),
        (object) axis
      },
      {
        "joyNum",
        (object) joystick
      }
    };

    private int Clamp(int value, int min, int max)
    {
      if (value < min)
        return min;
      return value > max ? max : value;
    }

    private float ParseFloat(string str, float defValue = 0.0f)
    {
      float result = defValue;
      return float.TryParse(str, NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out result) ? result : defValue;
    }

    private int ParseInt(string str, int defValue = 0)
    {
      int result = defValue;
      return int.TryParse(str, out result) ? result : defValue;
    }

    private InputType ParseInputType(int type, InputType defValue = InputType.Button)
    {
      switch (type)
      {
        case 0:
          return InputType.Button;
        case 1:
          return InputType.MouseAxis;
        case 2:
          return InputType.AnalogAxis;
        default:
          return defValue;
      }
    }
  }
}
