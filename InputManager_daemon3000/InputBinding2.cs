﻿// Decompiled with JetBrains decompiler
// Type: UnityInputConverter.InputBinding
// Assembly: UnityInputConverter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C78EFC52-C5FE-4647-973C-232B9F6B0029
// Assembly location: C:\Users\guilh\Desktop\InputManager-master\Assets\InputManager\Source\Editor\Library\UnityInputConverter.dll

using UnityEngine;

namespace UnityUtil.ModularInputSystem.InputManager_daemon3000
{
  internal class InputBinding2
  {
    public const float AXIS_NEUTRAL = 0.0f;
    public const float AXIS_POSITIVE = 1f;
    public const float AXIS_NEGATIVE = -1f;
    public const int MAX_MOUSE_AXES = 3;
    public const int MAX_JOYSTICK_AXES = 28;
    public const int MAX_JOYSTICKS = 11;
    public InputType Type;
    public KeyCode Positive;
    public KeyCode Negative;
    public float DeadZone;
    public float Gravity;
    public float Sensitivity;
    public bool Snap;
    public bool Invert;
    public int Axis;
    public int Joystick;

    public InputBinding2()
    {
      this.Type = InputType.Button;
      this.Positive = KeyCode.None;
      this.Negative = KeyCode.None;
      this.DeadZone = 0.0f;
      this.Gravity = 0.0f;
      this.Sensitivity = 0.0f;
      this.Snap = false;
      this.Invert = false;
      this.Axis = 0;
      this.Joystick = 0;
    }

    public static InputBinding2 Duplicate(InputBinding source) => new InputBinding2()
    {
      Positive = source.Positive,
      Negative = source.Negative,
      DeadZone = source.DeadZone,
      Gravity = source.Gravity,
      Sensitivity = source.Sensitivity,
      Snap = source.Snap,
      Invert = source.Invert,
      Type = source.Type,
      Axis = source.Axis,
      Joystick = source.Joystick
    };
  }
}
