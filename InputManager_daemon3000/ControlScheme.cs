﻿// Decompiled with JetBrains decompiler
// Type: UnityInputConverter.ControlScheme
// Assembly: UnityInputConverter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C78EFC52-C5FE-4647-973C-232B9F6B0029
// Assembly location: C:\Users\guilh\Desktop\InputManager-master\Assets\InputManager\Source\Editor\Library\UnityInputConverter.dll

using System;
using System.Collections.Generic;

namespace UnityUtil.ModularInputSystem.InputManager_daemon3000
{
  [System.Serializable]
  public class ControlScheme
  {
    public string Name;
    public string UniqueID;
    public List<InputAction> Actions;

    public ControlScheme()
      : this("New Controls Scheme")
    {
    }

    public ControlScheme(string name)
    {
      this.Name = name;
      this.UniqueID = Guid.NewGuid().ToString("N");
      this.Actions = new List<InputAction>();
    }
  }
}
